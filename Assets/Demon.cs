﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class Demon : MonoBehaviour
{
    private UnityEngine.AI.NavMeshAgent agent;
    private Transform player;
    private WaypointProgressTracker wpTracker;
    private Animator animator;
    private EyeSight eyeSight;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        //agent.isStopped = true;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        wpTracker = GetComponent<WaypointProgressTracker>();
        animator = GetComponent<Animator>();
        eyeSight = GetComponent<EyeSight>();
        //Invoke("StartMove", 5);
    }

    //Assign repeating but not frame-based tasks to coroutines and via invoking
    void Update()
    {
        if (this.eyeSight.CanSeeTarget())
        {
            Debug.Log("Can see target");
        }
    }
    
    void StartMove()
    {
        print("start move");
        animator.SetBool("walk", true);
        agent.isStopped = false;
        StartCoroutine("Howl");
        Move();
    }

    void Move()
    {
        //print("move");
        FollowWaypoints();
        Invoke("Move", 2);
    }

    void Stop()
    {
        animator.SetBool("walk", false);
        CancelInvoke();
        agent.isStopped = true;
        StopCoroutine("Howl");
    }

    void FollowPlayer()
    {
        agent.SetDestination(player.position);
    }
    
    void FollowWaypoints()
    {
        agent.SetDestination(wpTracker.target.position);        
    }

    IEnumerator Howl()
    {        
        for (; ; )
        {
            print("howling");            
            yield return new WaitForSeconds(UnityEngine.Random.Range(6f, 12f));
            Stop();
            EmitHowl();
        }
    }

    private void EmitHowl()
    {
        animator.SetTrigger("howl");
    }
}
