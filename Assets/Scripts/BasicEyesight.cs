﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEyesight : MonoBehaviour
{
    public int distance;
    public string target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        bool canSeeTarget = false;

        if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, distance))
        {
            Debug.Log("In front of " + hit.collider.gameObject.name);
            if (hit.collider.gameObject.name == target)
            {
                canSeeTarget = true;
            }
        }

        if (canSeeTarget)
        {
            GameObject.Find("GameManager").SendMessage("EnableLoader");
        }
        else
        {
            GameObject.Find("GameManager").SendMessage("DisableLoader");
        }
    }
}
