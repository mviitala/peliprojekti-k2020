﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

public class BasicZombie : MonoBehaviour
{
    void FollowPlayer()
    {

        GameObject Player = GameObject.FindWithTag("Player");

        AICharacterControl zombieCharacter = GetComponent<AICharacterControl>();
        zombieCharacter.target = Player.transform;
    }
}
