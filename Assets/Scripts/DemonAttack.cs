﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemonAttack : MonoBehaviour
{
    private Animator animator;
    public Rigidbody throwableWeapon;
    public int throwForce = 20;
    public Transform throwInitPosition;
    private bool isAttacking = false;
    private Transform target;
    private UnityEngine.AI.NavMeshAgent agent;

    private void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
    }

    private void Update()
    {
        if (isAttacking)
        {
            agent.SetDestination(this.target.position);
            agent.isStopped = false;
            animator.SetBool("walk", true);
        }
    }

    // Start is called before the first frame update
    public void Attack(Transform t)
    {
        this.target = t;
        //throwattack / melee attack
        this.isAttacking = true;
        animator.SetTrigger("attack");
    }

    // Update is called once per frame
    void ReleaseThrowable()
    {
        Rigidbody r = Instantiate(this.throwableWeapon, throwInitPosition.position, Quaternion.identity);
        Vector3 dir = (target.position - throwInitPosition.position).normalized;
        r.AddForce(dir * throwForce, ForceMode.Impulse);
        //r.AddForce(this.gameObject.transform.forward * throwForce, ForceMode.Impulse);
        this.isAttacking = false;
    }
}
