﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeSight : MonoBehaviour
{
    public Transform eyes;
    public Transform target;
    public float visionRange = 0;
    //-1 = distance ignored
    public float maxDistance = -1;
    //option to ignore y axis angle of the vision range
    public bool ignoreAngleY = false;

    public bool CanSeeTarget()
    {

        Vector3 tempTarget = target.position;
        if(this.ignoreAngleY)
            tempTarget.y = eyes.position.y;

        Debug.DrawLine(eyes.position, tempTarget, Color.yellow, 5);
        float dist = Vector3.Distance(eyes.position, tempTarget);
        Debug.Log("Distance to target: " + dist);

        if (this.maxDistance != -1 && dist > this.maxDistance)
        {
            return false;
        }     

        //direction between the eyes and the target
        Vector3 angleDirection = tempTarget - eyes.position;
        //check if the target is within field of vision
        Debug.Log("View angle to target: " + Vector3.Angle(angleDirection, eyes.forward));

        if (Vector3.Angle(angleDirection, eyes.forward) > visionRange)
        {
            return false;
        }
        else
        {
            //check if there are no obstacles between NPC and the target
            RaycastHit hit;
            Vector3 raycastDirection = target.position - eyes.position;
            if (Physics.Raycast(eyes.position, raycastDirection, out hit))
            {
                Debug.Log(hit.collider.gameObject.name);
                //if there are no obstacles between eyes and target (change tag, if necessary)
                if (hit.collider.gameObject.tag == "Player")
                {
                    return true;
                }
            }

        }

        return false;
    }

}
