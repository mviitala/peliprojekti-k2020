﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    public int health;
    public Slider healthSlider;
    public Image fillScript;
    public Color highHealthColor;
    public Color lowHealthColor;

    
    // Start is called before the first frame update
    void Start()
    {
        healthSlider.maxValue = this.health;
        healthSlider.minValue = 0;
        healthSlider.value = this.health;
        InvokeRepeating("DecreaseHealth", 2.0f, 0.5f);
        fillScript.color = this.highHealthColor;

    }

    // Update is called once per frame
    void DecreaseHealth()
    {
        fillScript.color = Color.Lerp(lowHealthColor, highHealthColor, (health / healthSlider.maxValue ));
        health -= 1;
        healthSlider.value = health;
    }
}
