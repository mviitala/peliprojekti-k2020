﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class Menu : MonoBehaviour
{

    bool menuOpen = false;
    public GameObject mainMenuPanel;
    public GameObject objectivesMenuPanel;
    public GameObject settingsPanel;

    public MusicPlayer musicPlayer;
    public PrefsHandler prefsHandler;
    public UnityEngine.UI.Slider musicSlider;
    public UnityEngine.UI.Slider effectsSlider;

    public RigidbodyFirstPersonController rbFpc;

    private void Start()
    {
        musicSlider.onValueChanged.AddListener(delegate { this.musicPlayer.setMusicVolume(musicSlider.value); });
        effectsSlider.onValueChanged.AddListener(delegate { this.musicPlayer.setEffectsVolume(effectsSlider.value); });
    }


    public void QuitGame()
    {

        prefsHandler.SavePrefs();

#if UNITY_EDITOR
        //emulate quit game in editor context
        UnityEditor.EditorApplication.isPlaying = false;
#endif
        Application.Quit();
    }

    public void ShowMainMenu()
    {

        //first, hide all panels
        HideAllMenus();
        //display the selected panel
        mainMenuPanel.SetActive(true);
    }

    public void ShowObjectivesPanel()
    {

        //first, hide all panels
        HideAllMenus();
        //display the selected panel
        objectivesMenuPanel.SetActive(true);
    }

    public void ShowSettingsPanel()
    {

        effectsSlider.value = prefsHandler.GetUserEffectsVolume();
        musicSlider.value = prefsHandler.GetUserMusicVolume();

        //first, hide all panels
        HideAllMenus();
        //display the selected panel
        settingsPanel.SetActive(true);
    }

    public void HideAllMenus()
    {

        mainMenuPanel.SetActive(false);
        objectivesMenuPanel.SetActive(false);
        settingsPanel.SetActive(false);
    }


    void Update()
    {

        //Listen for Escape
        if (Input.GetKeyDown(KeyCode.Escape))
        {

            //If menu is closed, open main menu
            if (!menuOpen)
            {
                this.rbFpc = GameObject.FindGameObjectWithTag("Player").GetComponent<RigidbodyFirstPersonController>();
                this.rbFpc.mouseLook.lockCursor = false;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                this.rbFpc.enabled = false;
                Time.timeScale = 0;

                ShowMainMenu();
                musicPlayer.SetPause();

            }
            //Else close all menus
            else
            {
                this.rbFpc.mouseLook.lockCursor = true;
                //Does not work - a bug?
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
                this.rbFpc.enabled = true;
                Time.timeScale = 1;

                HideAllMenus();
                musicPlayer.SetPlay();
 
            }

            //false to true or true to false
            menuOpen = !menuOpen;
        }
    }




}
