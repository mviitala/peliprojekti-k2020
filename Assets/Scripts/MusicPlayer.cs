﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicPlayer : MonoBehaviour
{
    public AudioMixer mixer;
    public AudioMixerSnapshot pauseSnapshot;
    public AudioMixerSnapshot gameSnapshot;

    public PrefsHandler prefsHandler;

    public void Start()
    {
        LoadUserVolumePrefs();
    }

    public void LoadUserVolumePrefs()
    {
        mixer.SetFloat("userMusicVolume", prefsHandler.GetUserMusicVolume());
        mixer.SetFloat("userEffectsVolume", prefsHandler.GetUserEffectsVolume());
    }

    public void setMusicVolume(float vol)
    {
        print("Volume: " + vol);
        mixer.SetFloat("userMusicVolume", vol);
        prefsHandler.SetUserMusicVolume(vol);
    }

    public void setEffectsVolume(float vol)
    {
        print("Volume: " + vol);
        mixer.SetFloat("userEffectsVolume", vol);
        prefsHandler.SetUserEffectsVolume(vol);
    }

    public void SetPlay()
    {
        gameSnapshot.TransitionTo(0.6f);
    }

    public void SetPause()
    {
        pauseSnapshot.TransitionTo(0.6f);
    }

}
