﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefsHandler : MonoBehaviour
{

    float userMusic;
    float userEffects;

    void Start()
    {

        LoadPrefs();
    }

    public void LoadPrefs()
    {

        Debug.Log("load prefs");
        userMusic = PlayerPrefs.GetFloat("userMusicVolume");
        userEffects = PlayerPrefs.GetFloat("userEffectsVolume");
    }

    public void SavePrefs()
    {

        Debug.Log("save prefs");
        PlayerPrefs.SetFloat("userMusicVolume", userMusic);
        PlayerPrefs.SetFloat("userEffectsVolume", userEffects);
    }

    public void SetUserMusicVolume(float vol)
    {

        userMusic = vol;
    }

    public void SetUserEffectsVolume(float vol)
    {

        userEffects = vol;
    }

    public float GetUserMusicVolume()
    {

        return userMusic;
    }

    public float GetUserEffectsVolume()
    {

        return userEffects;
    }

}
