﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomAudioPlayer : MonoBehaviour {

    public AudioClip[] audioClips;
	public AudioSource audioSource;

	// Use this for initialization
	void Start () {

		audioSource = GetComponent<AudioSource>();
		PlayAudio();
	}
		
	void PlayAudio () {

		int randomNumber = Random.Range (0, audioClips.Length);
		AudioClip randomClip = audioClips [randomNumber];
        //asetetaan taulukosta haettu AudioClip AudioSource-komponentin toistettavaksi
		audioSource.clip = randomClip;
        //käynnistetään toisto
		audioSource.Play ();
        //viivästytetty funktiokutsu (rekursio)
		Invoke ("PlayAudio", Random.Range(3, 10));
	}
}
