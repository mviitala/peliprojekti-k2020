﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RigidbodyPusher : MonoBehaviour
{
    public int pushPower;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Debug.Log("Push");
            Collider[] hitColliders = Physics.OverlapSphere(transform.position, 2);
            int i = 0;
            while (i < hitColliders.Length)
            {
                if(hitColliders[i].tag == "Pushable")
                {
                    Rigidbody body = hitColliders[i].attachedRigidbody;
                    Vector3 pushDir = transform.TransformDirection(Vector3.forward);
                    body.velocity = pushDir * pushPower;
                }                               
                i++;
            }
        }
    }
}
