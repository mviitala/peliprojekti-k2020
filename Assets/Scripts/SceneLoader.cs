﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string targetScene;
    private float startTime = 0f;
    private float timer = 0f;
    public float holdTime = 2.0f;
    // Use if you only want to call the method once after holding for the required time
    private bool held = false;
    private bool loadEnabled;
    public string key = "e";
    public GameObject loadGUI;

    private void Start()
    {
        loadEnabled = false;
    }

    public void EnableLoader()
    {
        loadEnabled = true;
        loadGUI.SetActive(true);
    }

    public void DisableLoader()
    {
        loadEnabled = false;
        loadGUI.SetActive(false);
    }


    void Update()
    {
        if (!loadEnabled) return;

        // Starts the timer from when the key is pressed
        if (Input.GetKeyDown(key))
        {
            startTime = Time.time;
            timer = startTime;
            Debug.Log("Key down");
        }

        //For testing
        if (Input.GetKeyUp(key) && held == false)
        {
            Debug.Log("Key up");
        }

        // Adds time onto the timer so long as the key is pressed
        if (Input.GetKey(key) && held == false)
        {
            timer += Time.deltaTime;

            // Once the timer float has added on the required holdTime, changes the bool (for a single trigger), and calls the function
            if (timer > (startTime + holdTime))
            {
                held = true;
                Debug.Log("held for " + holdTime + " seconds");
                LoadScene();
            }
        }
    }

    void LoadScene()
    {
        SceneManager.LoadScene(targetScene);
    }
}
