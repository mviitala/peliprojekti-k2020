﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieTrigger : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.tag == "Player")
        {
            GameObject[] zombies;
            zombies = GameObject.FindGameObjectsWithTag("Zombie");

            foreach (GameObject z in zombies)
            {
                z.SendMessage("FollowPlayer");
            }
        }
    }
}
